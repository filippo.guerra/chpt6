# Chpt6

In this repository, there are the tables with information regarding Chapter 6 "Intersex is not a component of the Nasonia vitripennis sex-determination cascade" of the thesis "The role of doublesex in shaping temporal, spatial and species-specific sex differentiation in Nasonia wasps"

Supplementary Table 1: primers used in qPCR-mediated gene expression analysis. Annealing temperature used in qPCr is reported in the last column (T [°C]).

Supplementary Table 2: primers used in dsRNA template generation
